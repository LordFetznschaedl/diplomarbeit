# Diplomarbeit

##Speicherplatz für der Messdaten in der Datenbank:    
	10900 Datenbankeinträge = 458752 Byte = 448 KB = 0,4375 MB     
	1 Datenbankeintrag ~ 42 Byte     
	2880 Datenbankeinträge ~ 1 Tag     
	10900 Datenbankeinträge ~ 3,785 Tage     
	365 Tage ~ 1051200 Datenbankeinträge     
	1051200 Datenbankeinträge ~ 44242211 Byte = 43205 KB = 42,193 MB      
	365 Tage ~ 42,193 MB       
	   
##Server:      
	Ip vom Raspi   
	Port: 1337   
	Datenformat: Datum;Temperatur;Luftfeuchtigkeit  
	Protokoll: TCP 
	Sprache: C#
	   
##WebApp:
	Darstellung mit Chart.js   
	Ports: 5000 (http), 5001 (https)   
	Links:
	* http://localhost:5000/
	* https://localhost:5001/
	Sprache: C#
	
##Client 
	Protokoll: TCP
	Sprache: C#
	
##messen.py
	GPIO: 4
	Sprache: Python
	
##setup.py
	Sprache: Python
	
##delete.py
	Sprache: Python
	
	


	
	



