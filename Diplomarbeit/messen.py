#!/usr/bin/python

#IMPORTS

import sys
import Adafruit_DHT
import time
import mysql.connector
from mysql.connector import errorcode


#VARIABLEN

connection = None
datenbankname = "Messungen"
user = "root"
password = "root"
host = "127.0.0.1"


#METHODEN

def messen():
	date = time.strftime('%Y-%m-%d %H:%M:%S')
	print(date)
	humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.AM2302,4)
	if humidity == None and temperature == None:
		return -2
	if humidity > 100:
		print("Messung fehlgeschlagen! Erneuter Messung in 10 Sekunden ...")
		time.sleep(10)
		return -1
	print("Temperatur = {0:0.2f}* - Luftfeuchtigkeit =  {1:0.2f}%".format(temperature, humidity))
	speichern("data", (date, "{0:0.2f}".format(temperature), 1))
	speichern("data", (date, "{0:0.2f}".format(humidity), 2))
	print("Messung hat funktioniert")
	return 1

def speichern(table, data):
	cursor = connection.cursor()
	cursor.execute("INSERT INTO "+table+" (datetime, value, id) VALUES (%s, %s, %s)", data)
	connection.commit()
	cursor.close()


#PROGRAMM

if len(sys.argv) < 2:
	print("ZU WENIG PARAMETER")
        print("ES WIRD EIN PARAMETER FUER DIE ZEIT ZWISCHEN DEN MESSUNGEN BENOETIGT")
        sys.exit(1)
elif len(sys.argv) > 2:
        print("ZU VIELE PARAMETER")
        print("ES WIRD EIN PARAMETER FUER DIE ZEIT ZWISCHEN DEN MESSUNGEN BENOETIGT")
        sys.exit(1)

try:
	print("Verbindung zu Datenbank "+datenbankname+" wird aufgebaut")
	connection = mysql.connector.connect(user=user, password=password, host=host, database=datenbankname)
	print("Verbindung aufgebaut")

except mysql.connector.Error as e:
        if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
		print("LOGINDATEN SIND FALSCH")
        elif e.errno == errorcode.ER_BAD_DB_ERROR:
                print("DATENBANK EXISTIERT NICHT")
        else:
                print(str(e))
        sys.exit(1)
except:
      	print("ERROR: "), sys.exc_info()[0]



while(True):

	try:
		returnValue = messen()
		if returnValue  == -1:
			print("Messung wird erneut versuchen!")
			messen()
		elif returnValue == -2:
			print("SENSOR IST NICHT RICHTIG ANGESCHLOSSEN / KEIN SENSOR VORHANDEN")

	except IOError as e:
		print("IO-ERROR ({0}): {1}").format(e.erno, e.strerror)
	except ValueError as e:
		print("VALUE-ERROR ("+str(e)+")")
	except NameError as e:
		print("NAME-ERROR ("+str(e)+")")
	except IndexError as e:
		print("INDEX-ERROR ("+str(e)+")")
	except KeyboardInterrupt:
		print("PROGRAMM WURDE ABGEBROCHEN!")
		break
	except ImportError as e:
		print("IMPORT-ERROR ("+str(e)+")")
	except TypeError as e:
		print("TYPE-ERROR("+str(e)+")")
	except:
		print("ERROR: "), sys.exc_info()[0]

	time.sleep(float(sys.argv[1]))

connection.close()
sys.exit(0)
