﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Models
{
   


    public class Messwerte
    {
        private float temperature { get => _temperature; set => _temperature = value; }
        private float humidity { get => _humidity; set => _humidity = value; }
        private string label { get => _label; set => _label = value; }

        private float _temperature;
        private float _humidity;
        private string _label;

        //Daten der letzten 24 Stunden
        private List<Messwerte> listMesswerte24h = new List<Messwerte>();

        public void addMesswerte24h(float temp, float hum, string label)
        {
            listMesswerte24h.Add(new Messwerte { temperature = temp, humidity = hum, label = label });
        }

        public void ersetzeAeltestenWert24h(float temp, float hum, string label)
        {
            addMesswerte24h(temp, hum, label);
            listMesswerte24h.RemoveAt(0);
        }

        public float getTempList24h(int i)
        {
            return listMesswerte24h[i]._temperature;
        }

        public float getHumList24h(int i)
        {
            return listMesswerte24h[i]._humidity;
        }

        public string getLabel24h(int i)
        {
            return listMesswerte24h[i]._label;
        }

        //Daten der letzten Stunde
        private List<Messwerte> listMesswerte1h = new List<Messwerte>();

        public void addMesswerte1h(float temp, float hum, string label)
        {
            listMesswerte1h.Add(new Messwerte { temperature = temp, humidity = hum, label = label });

        }

        public void ersetzeAeltestenWert1h(float temp, float hum, string label)
        {
            addMesswerte1h(temp, hum, label);
            listMesswerte1h.RemoveAt(0);
        }

        public float getTempList1h(int i)
        {
            return listMesswerte1h[i]._temperature;
        }

        public float getHumList1h(int i)
        {
            return listMesswerte1h[i]._humidity;
        }

        public string getLabel1h(int i)
        {
            return listMesswerte1h[i]._label;
        }

        //Singleton
        private static Messwerte instance;
        public static Messwerte getInstance()
        {
            if (instance == null)
            {
                instance = new Messwerte();
            }
            return instance;
        }

        //Variablen
        private NumberFormatInfo format = new NumberFormatInfo();

        private float temp;
        private float hum;
        private string labelS;

        private MySqlConnection connection;

        private readonly string user = "root";
        private readonly string passwort = "root";
        private readonly string host = "localhost";
        private readonly string port = "3306";
        private readonly string datenbankname = "Messungen";

        private MySqlCommand commandT;
        private MySqlCommand commandH;
        private MySqlCommand commandTime;
        private MySqlDataReader readerT;
        private MySqlDataReader readerH;
        private MySqlDataReader readerTime;

        public void readAvgYesterday(int h)
        {

            string connStr = "User ID=" + user + ";Password=" + passwort + "; Host=" + host + "; Port=" + port + "; Database=" + datenbankname + "; SSL Mode=None";

            try
            {

                connection = new MySqlConnection(connStr);
                connection.Open();

                DateTime yesterday = DateTime.Today.AddDays(-1);
                string yesterdayS = yesterday.ToString("yyyy-MM-dd");

                for (int i = h + 1; i <= 23; i++)
                {
                    try
                    {
                        string sqlT = "select avg(value) from data where datetime >= '" + yesterdayS + " " + i + ":00:00' and datetime <= '" + yesterdayS + " " + i + ":59:59' and id=1";
                        string sqlH = "select avg(value) from data where datetime >= '" + yesterdayS + " " + i + ":00:00' and datetime <= '" + yesterdayS + " " + i + ":59:59' and id=2";

                        labelS = yesterday.ToString("dd-MM-yyyy") + " - " + i + " Uhr";

                        commandT = new MySqlCommand(sqlT, connection);
                        readerT = commandT.ExecuteReader();
                        var sbT = new StringBuilder();
                        while (readerT.Read())
                        {

                            sbT.AppendLine(readerT[0].ToString());
                        }

                        temp = parse(sbT.ToString());
                        readerT.Close();

                        commandH = new MySqlCommand(sqlH, connection);
                        readerH = commandH.ExecuteReader();
                        var sbH = new StringBuilder();
                        while (readerH.Read())
                        {

                            sbH.AppendLine(readerH[0].ToString());
                        }

                        hum = parse(sbH.ToString());
                        readerH.Close();

                        ersetzeAeltestenWert24h(temp, hum, labelS);
                    }
                    catch (Exception)
                    {
                        continue;
                    }

                }

                connection.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:

                        Console.WriteLine("Verbindung zum Server nicht aufbaubar!");
                        break;
                    case 1045:
                        Console.WriteLine("Falscher Nutzername / Passwort");
                        break;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Format Fehler");

            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Verbindung nicht valid oder geöffnet");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Null Reference Exception");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void readAvgToday(int h)
        {

            string connStr = "User ID=" + user + ";Password=" + passwort + "; Host=" + host + "; Port=" + port + "; Database=" + datenbankname + "; SSL Mode=None";

            try
            {
                connection = new MySqlConnection(connStr);
                connection.Open();

                DateTime today = DateTime.Now;
                string todayS = today.ToString("yyyy-MM-dd");

                for (int i = 0; i <= h; i++)
                {

                    string sqlT = "select avg(value) from data where datetime >= '" + todayS + " " + i + ":00:00' and datetime <= '" + todayS + " " + i + ":59:59' and id=1";
                    string sqlH = "select avg(value) from data where datetime >= '" + todayS + " " + i + ":00:00' and datetime <= '" + todayS + " " + i + ":59:59' and id=2";

                    labelS = today.ToString("dd-MM-yyyy") + " - " + i + " Uhr";

                    commandT = new MySqlCommand(sqlT, connection);
                    readerT = commandT.ExecuteReader();
                    var sbT = new StringBuilder();
                    while (readerT.Read())
                    {

                        sbT.AppendLine(readerT[0].ToString());
                    }

                    temp = parse(sbT.ToString());
                    readerT.Close();

                    commandH = new MySqlCommand(sqlH, connection);
                    readerH = commandH.ExecuteReader();
                    var sbH = new StringBuilder();
                    while (readerH.Read())
                    {

                        sbH.AppendLine(readerH[0].ToString());
                    }

                    hum = parse(sbH.ToString());
                    readerH.Close();

                    ersetzeAeltestenWert24h(temp, hum, labelS);
                }

                connection.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Verbindung zum Server nicht aufbaubar!");
                        break;
                    case 1045:
                        Console.WriteLine("Falscher Nutzername / Passwort");
                        break;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Format Fehler");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Verbindung nicht valid oder geöffnet");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Null Reference Exception");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void readHour()
        {

            string connStr = "User ID=" + user + ";Password=" + passwort + "; Host=" + host + "; Port=" + port + "; Database=" + datenbankname + "; SSL Mode=None";

            connection = new MySqlConnection(connStr);
            try
            {

                connection.Open();


                string sqlT = "select value from data where id=1 order by datetime desc limit 60";
                string sqlH = "select value from data where id=2 order by datetime desc limit 60";
                string sqlTime = "select datetime from data where id=1 order by datetime desc limit 60";

                List<string> tempList = new List<string>();
                List<string> humList = new List<string>();
                List<string> timeList = new List<string>();

                commandT = new MySqlCommand(sqlT, connection);
                readerT = commandT.ExecuteReader();

                while (readerT.Read())
                {

                    tempList.Add(readerT[0].ToString());
                }

                readerT.Close();

                commandH = new MySqlCommand(sqlH, connection);
                readerH = commandH.ExecuteReader();

                while (readerH.Read())
                {

                    humList.Add(readerH[0].ToString());
                }

                readerH.Close();

                commandTime = new MySqlCommand(sqlTime, connection);
                readerTime = commandTime.ExecuteReader();

                while (readerTime.Read())
                {

                    timeList.Add(readerTime[0].ToString());
                }

                readerTime.Close();

                tempList.Reverse();
                humList.Reverse();
                timeList.Reverse();



                for (int i = 0; i < 60; i++)
                {

                    temp = parse(tempList[i]);
                    hum = parse(humList[i]);
                    labelS = timeList[i];


                    ersetzeAeltestenWert1h(temp, hum, labelS);


                }

                connection.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:

                        Console.WriteLine("Verbindung zum Server nicht aufbaubar!");
                        break;
                    case 1045:
                        Console.WriteLine("Falscher Nutzername / Passwort");
                        break;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Format Fehler");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Verbindung nicht valid oder geöffnet");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Null Reference Exception");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());

            }
        }

        private float parse(string s)
        {
            format.NumberDecimalSeparator = ",";
            format.NumberDecimalDigits = 1;

            if (s.Contains(','))
            {
                return float.Parse(s.ToString().Substring(0, s.ToString().IndexOf(',') + 2), format);
            }
            else
            {
                return float.Parse(s.ToString(), format);
            }
        }

    }
}
