﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;


namespace WebApp.Controllers
{
    public class Data60Controller : Controller
    {
        public static bool b = false;

        List<float> tempList = new List<float>();
        List<float> humList = new List<float>();
        List<string> labelList = new List<string>();

        public IActionResult Index()
        {



            if (b == false)
            {

                for (int i = 0; i < 60; i++)
                {
                    Messwerte.getInstance().addMesswerte1h(0, 0, "FUELLER");
                }

               
                


                b = true;
            }
            else
            {
                for (int i = 0; i < 60; i++)
            {
                Messwerte.getInstance().ersetzeAeltestenWert1h(0, 0, "FUELLER");
            }
            }

            

            Messwerte.getInstance().readHour();


            for (int i = 0; i < 60; i++)
            {
                tempList.Add(Messwerte.getInstance().getTempList1h(i));
                humList.Add(Messwerte.getInstance().getHumList1h(i));
                labelList.Add(Messwerte.getInstance().getLabel1h(i));
            }



            ViewBag.temp = tempList;
            ViewBag.hum = humList;
            ViewBag.label = labelList;


            return View();

        }

        public IActionResult ReloadData()
        {

            ViewData["Message"] = "reloading Data ...";

            

            Console.WriteLine("RELOAD-60");
            Response.Redirect("/Data60/");
            Index();


            return View();
        }
    }




}
