﻿using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using System;
using System.Collections.Generic;


namespace WebApp.Controllers
{
    public class DataController : Controller
    {
        public static bool b = false;

        List<float> tempList = new List<float>();
        List<float> humList = new List<float>();
        List<string> labelList = new List<string>();

        public IActionResult Index()
        {
            if (b == false)
            {
                for(int i=0;i<24;i++)
                {
                    Messwerte.getInstance().addMesswerte24h(0, 0, "FUELLER");
                }
                b = true;
            }
            else
            {
                for (int i = 0; i < 24; i++)
                {
                    Messwerte.getInstance().ersetzeAeltestenWert24h(0, 0, "FUELLER");
                }
            }
            Messwerte.getInstance().readAvgYesterday(DateTime.Now.AddHours(-1).Hour);
            Messwerte.getInstance().readAvgToday(DateTime.Now.AddHours(-1).Hour);

            for (int i = 0; i < 24; i++)
            {
                tempList.Add(Messwerte.getInstance().getTempList24h(i));
                humList.Add(Messwerte.getInstance().getHumList24h(i));
                labelList.Add(Messwerte.getInstance().getLabel24h(i));
            }

            ViewBag.temp = tempList;
            ViewBag.hum = humList;
            ViewBag.label = labelList;

            return View();
        }


        public IActionResult ReloadData()
        {

            ViewData["Message"] = "reloading Data ...";

            
            Console.WriteLine("RELOAD-24");
            Response.Redirect("/Data/");
            Index();

            return View();
        }
    }
}
