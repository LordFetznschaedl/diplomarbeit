﻿
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Server
{
    class Program
    {
        private const int portNum = 1337;
        private static IPAddress address = IPAddress.Parse("192.168.73.104");
        private static byte[] bytedata;


        public static void Main(String[] args)
        {
            bool done = false;

            TcpListener listener = new TcpListener(address,portNum);

            listener.Start();

            while (!done)
            {
                Console.WriteLine("Waiting for connection...");
                TcpClient client = listener.AcceptTcpClient();

                Console.WriteLine("Connection accepted!");
                NetworkStream ns = client.GetStream();

                MySqlConnection connection;
                MySqlCommand command;
                string user = "root";
                string passwort = "root";
                string host = "localhost";
                string port = "3306";
                string datenbankname = "Messungen";

                
                
            
                MySqlDataReader reader;

                string connStr = "User ID=" + user + ";Password=" + passwort + ";Host=" + host + ";Port=" + port + ";Database=" + datenbankname + "; SSL Mode=None";
                connection = new MySqlConnection(connStr);
                try
                {

                    

                    Console.WriteLine("Verbinden zur Datenbank wird hergestellt...");
                    connection.Open();
                    Console.WriteLine("Verbindung wurde hergestellt");

                    

                   

                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    switch (ex.Number)
                    {
                        case 0:
                            Console.WriteLine("Verbindung zum Server nicht aufbaubar.");
                            break;
                        case 1045:
                            Console.WriteLine("Falscher Nutzername / Passwort");
                            break;

                    }
                }

                try
                {
                    string sql = "select * from data";

                    command = new MySqlCommand(sql, connection);
                    reader = command.ExecuteReader();
                    var sb = new StringBuilder();
                    while (reader.Read())
                    {
                        sb.AppendLine(reader[0] + ";" + reader[1] + ";" + reader[2]);
                        
                    }
                    bytedata = Encoding.ASCII.GetBytes(sb.ToString());

                    ns.Write(bytedata,0,bytedata.Length);
                

                    Console.WriteLine("Übertragung beendet");

                    reader.Close();
                    ns.Close();
                    client.Close();
                    connection.Close();
                    Console.WriteLine("Alle Verbindung geschlossen");
                    Console.WriteLine("-------------------------------");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            listener.Stop();

            
        }

    }
}
