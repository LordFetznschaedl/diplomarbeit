﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    class Program
    {
        private const int portNum = 1337;
        private const string hostName = "192.168.73.104";

        public static void Main(String[] args)
        {
            getData();
          


            Console.ReadLine();
        }

        public static void getData()
        {
            try
            {
                TcpClient client = new TcpClient(hostName, portNum);
                NetworkStream ns = client.GetStream();

                do
                {
                    byte[] bytes = new byte[client.Available];
                    int bytesRead = ns.Read(bytes, 0, bytes.Length);

                    File.AppendAllText("/Users/p01/Desktop/Diplomarbeit/data.csv", Encoding.ASCII.GetString(bytes, 0, bytesRead));
                    Console.WriteLine(Encoding.ASCII.GetString(bytes, 0, bytesRead));

                } while (ns.DataAvailable);

                client.Close();
                
                

                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
