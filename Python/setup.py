#!/usr/bin/python

#IMPORTS

import sys
import mysql.connector
from mysql.connector import errorcode


#VARIABLEN

connection = None
datenbankname = "Messungen"
user = "root"
password = "root"
host = "localhost"

TABLES = {}
TABLES['relation'] = (
                      "CREATE TABLE relation("
                      "id INT PRIMARY KEY NOT NULL,"
                      "type TEXT NOT NULL)")

TABLES['data'] = (
                  "CREATE TABLE data("
                  "datetime DATETIME NOT NULL,"
                  "value FLOAT(4,1) SIGNED NOT NULL,"
                  "id INT NOT NULL,"
                  "FOREIGN KEY (id) REFERENCES relation(id))")

insertRelation = ("INSERT INTO relation (id, type) VALUES (%(id)s, %(type)s)")

dataTemperature = {
    'id' : 1,
        'type' : "Temperatur",
}

dataHumidity = {
    'id' : 2,
        'type' : "Luftfeuchtigkeit",
}

#METHODEN

def createDatabase(databaseName):
    cursor = connection.cursor()
    cursor.execute("CREATE DATABASE {0}".format(databaseName))
    cursor.close()

def createTable(createStatement):
    cursor = connection.cursor()
    cursor.execute(createStatement)
    cursor.close()

def insert(insertStatement, data):
    cursor = connection.cursor()
    cursor.execute(insertStatement, data)
    cursor.close()

#PROGRAMM

print("DATENBANK SETUP")

try:
    print("Verbindung zu Datenbankserver wird aufgebaut")
    connection = mysql.connector.connect(user=user, password=password, host=host)
    print("Verbindung aufgebaut")
    print("Verbindung zu Datenbank "+datenbankname+" wird aufgebaut")
    connection.database = datenbankname
    print("Verbindung hergestellt")

except mysql.connector.Error as e:
    if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("LOGINDATEN SIND FALSCH")
    elif e.errno == errorcode.ER_BAD_DB_ERROR:
        createDatabase(datenbankname)
        print("DATENBANK ERSTELLT")
        connection.database = datenbankname
        print("Verbindung hergestellt")
    else:
        print(str(e))
except:
    print("ERROR: "), sys.exc_info()[0]

for name, ddl in TABLES.iteritems():
    try:
        print("Erstelle Tabelle {0}: ".format(name))
        createTable(ddl)
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("TABELLE EXISTIERT SCHON")
        else:
            print(str(e))
    except:
        print("ERROR: "), sys.exc_info()[0]
    else:
        print("Erstellt!")

try:
    insert(insertRelation,dataTemperature)
    insert(insertRelation,dataHumidity)
except mysql.connector.IntegrityError as e:
    print("Daten schon hinzugefuegt!")
except:
    print("ERROR: "), sys.exc_info()[0]
else:
    print("Daten hinzugefuegt!")


connection.commit()

connection.close()

sys.exit(0)

