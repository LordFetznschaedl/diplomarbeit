
#!/usr/bin/python

#IMPORTS

import sys
import mysql.connector
from mysql.connector import errorcode
import datetime


#VARIABLEN

connection = None
datenbankname = "Messungen"
user = "root"
password = "root"
host = "127.0.0.1"


#METHODEN

def loeschen(table, date):
	cursor = connection.cursor()
	cursor.execute("delete from "+table+" where datetime < \'"+date+"\'")
	connection.commit()
	cursor.close()


#PROGRAMM


try:
	print("Verbindung zu Datenbank "+datenbankname+" wird aufgebaut")
	connection = mysql.connector.connect(user=user, password=password, host=host, database=datenbankname)
	print("Verbindung aufgebaut")
	date = datetime.datetime.now() - datetime.timedelta(days=365)
	loeschen("data", date.strftime('%Y-%m-%d %H:%M:%S'))
	print("DATEN GELOESCHT!")

except mysql.connector.Error as e:
        if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
		print("LOGINDATEN SIND FALSCH")
        elif e.errno == errorcode.ER_BAD_DB_ERROR:
                print("DATENBANK EXISTIERT NICHT")
        else:
                print(str(e))
except IOError as e:
	print("IO-ERROR ({0}): {1}").format(e.erno, e.strerror)
except ValueError as e:
	print("VALUE-ERROR ("+str(e)+")")
except NameError as e:
	print("NAME-ERROR ("+str(e)+")")
except IndexError as e:
	print("INDEX-ERROR ("+str(e)+")")
except KeyboardInterrupt:
	print("PROGRAMM WURDE ABGEBROCHEN!")
except ImportError as e:
	print("IMPORT-ERROR ("+str(e)+")")
except TypeError as e:
	print("TYPE-ERROR("+str(e)+")")
except:
	print("ERROR: "), sys.exc_info()[0]

connection.close()
sys.exit(0)
